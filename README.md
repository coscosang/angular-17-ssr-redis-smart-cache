# Angular 17 SSR - Умное кеширование с Redis и без

## Описание
Модифицированный server.ts для кеширования сервер рендеринга Angular 17 


Есть поддержка Redis, если отключен будет харниться просто в Map, что не рекомендуется для больших проектов.


Умное кеширование заключается в том, что кеш автоматически обновляется после отдачи страницы из кеша, что дает практически всегда актуальный кеш html страниц и моментальную отдачу сайта.
Так же кеш устанавливается на сутки для страниц, что бы не отдавать совсем старые страницы при первом хите после долгого отсутствия посещений.


## Зависимости
- lodash - Для копирвоания объекта res что бы при отдаче кеша ангуляр смог установить статус страницы и записать в кеш его обнвленный:
```bash
npm i redis --force
npm i lodash.clone --force
npm i --save-dev @types/lodash --force
```


## Установка

Просто скопируйте в корень своего проекта заменив файлы


## Особенности
[express.token.ts](src%2Fexpress.token.ts) - На свое усмотрение можно выдерунть, если без него нормально работает установка статуса внутри ангуляр
Мне же пришлось использовать в компоненте 404

```typescript
export class Page404Component implements OnInit {
    private readonly response?: Response;
    constructor(
        @Optional() @Inject(RESPONSE) response: any
    ) {
        this.response = response;
    }
    ngOnInit() {
      if (this.response) {
        this.response.status(404);
      }
    }
}
```



## Redis - Docker compose

Его можно понять в контейнере докера

PASSWORD - Заменить на свой и в файле [cache.server.ts](cache.server.ts) тоже

```yaml
version: '3.1'
services:
  redis:
    image: "redis:latest"
    container_name: "redis"
    privileged: true
    ports:
      - "6379:6379"
    networks:
      - default # укажите свой
    tty: true
    restart: always
    sysctls:
      - net.core.somaxconn=511
    deploy:
      resources:
        limits:
          memory: 5g
    # command: ["redis-server", "--save", "3600", "1"]
    command: ["redis-server", "--requirepass PASSWORD"] #PASSWORD - Заменить
    volumes:
      - redis_data:/data
volumes:
  redis_data:
```













# Angular 17 SSR - Redis smart cache

## Description
Modified `server.ts` for caching server-side rendering in Angular 17.

Supports Redis. If Redis is disabled, it will store in a Map, which is not recommended for large projects.

Smart caching automatically updates the cache after serving the page from the cache, providing almost always up-to-date cached HTML pages and instant site delivery. The cache is also set for a day for pages to avoid serving very old pages on the first hit after a long absence of visits.

## Dependencies
- lodash: For copying the `res` object so Angular can set the page status and write its updated version to the cache:
```bash
npm i redis --force
npm i lodash.clone --force
npm i --save-dev @types/lodash --force
```

## Installation

Simply copy to the root of your project, replacing files.


## Features
- [express.token.ts](src%2Fexpress.token.ts): Optionally, you can remove it if setting the status inside Angular works fine without it. I had to use it in the 404 component:

```typescript
export class Page404Component implements OnInit {
    private readonly response?: Response;
    constructor(
        @Optional() @Inject(RESPONSE) response: any
    ) {
        this.response = response;
    }
    ngOnInit() {
      if (this.response) {
        this.response.status(404);
      }
    }
}
```

## Redis - Docker Compose

You can run it in a Docker container.

Replace `PASSWORD` with your password and also in the file [cache.server.ts](cache.server.ts).

```yaml
version: '3.1'
services:
  redis:
    image: "redis:latest"
    container_name: "redis"
    privileged: true
    ports:
      - "6379:6379"
    networks:
      - default # specify your network
    tty: true
    restart: always
    sysctls:
      - net.core.somaxconn=511
    deploy:
      resources:
        limits:
          memory: 1g
    # command: ["redis-server", "--save", "3600", "1"]
    command: ["redis-server", "--requirepass PASSWORD"] #PASSWORD - Replace
    volumes:
      - redis_data:/data
volumes:
  redis_data:
