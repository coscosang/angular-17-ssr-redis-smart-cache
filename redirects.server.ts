export class RedirectsServer {
  public baseRedirects(res: any, req: any): boolean {
    const {protocol, originalUrl, baseUrl, headers} = req;
    const host = req.get('host');
    ////// Удаляем index.php или index.html и выполняем редирект
    if (originalUrl.endsWith('index.php')) {
      let newUrl = originalUrl.replace('index.php', '');
      return res.redirect(301, `https://${host}${newUrl}`);
    }
    if (originalUrl.endsWith('index.html')) {
      let newUrl = originalUrl.replace('index.html', '')
      return res.redirect(301, `https://${host}${newUrl}`);
    }


    ////// WWWW
    // Если URL начинается с www, редиректим на версию без www
    if (host && host.startsWith('www.')) {
      return res.redirect(301, `https://${host.replace('www.', '')}${originalUrl}`);
    }


    ////// 404
    const is404Page = req.path === '/404';
    if (is404Page) {
      // Ставим принудительно 404 на всякий если урл 404 страницы
      res.status(404);
    }
    return false;
  }
  addTrailingSlash(res: any, req: any): boolean {
    const hasTrailingSlash = req.path.endsWith('/');
    if (!hasTrailingSlash) {
      // Редиректим на страницу со слешем в конце для СЕО
      res.redirect(301, `${req.path}/`);
      return true;
    }
    return false;
  }

}







