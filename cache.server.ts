import { createClient } from 'redis';


export class CacheServer {
  private expiredTime = 86000;
  private debug = true;
  public httpHtmlCache: Map<string, any> = new Map<string, any>();
  public redisClient = createClient({
    url: 'redis://default:PASSWORD@redis:6379'
  });

  constructor() {
    (async () => {
      await this.redisClient.connect();
    })();
  }

  async setCache(key: string, html: string, statusCode: number) {
    if (this.redisClient?.isReady) {
      const data = {html: html, statusCode: statusCode};
      await this.redisClient.set(key, JSON.stringify(data),  {
        EX: this.expiredTime
      })
      this.log('Set cache REDIS', key, html.length, statusCode)
    }else{
      this.httpHtmlCache.set(key, {html: html, statusCode: statusCode});
      console.warn('Please configure redis in server.ts!')
    }
  }
  async getCache(key: string) {
    if (this.redisClient?.isReady) {
      const cache = await this.redisClient.get(key);
      this.log('Get cache REDIS', key, cache?.length)
      return cache ? JSON.parse(cache) : null;
    }else{
      return this.httpHtmlCache.get(key);
    }
  }

  private log(...text: any) {
    if(this.debug) {
      console.log(...text)
    }
  }

}







